import subprocess

def new_video():
    subprocess.call('ffmpeg -i video.mp4 -i audio.mp3 -map 0:v -map 1:a -c:v copy -shortest output.mp4', shell=True)
    subprocess.call('ffmpeg -i output.mp4 -vf subtitles=sub.srt result.mp4', shell=True)

if __name__ == "__main__":
    new_video()